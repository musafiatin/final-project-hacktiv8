#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#define nMax 30

//Struct Nasabah
struct accountNasabah {
    char inpName[20];
    char inpTlp[15];
    char inpAddr[40];
    int inpPin;
    int cardNumber;

    struct accountNasabah *next;
} *head, *tail, *current;

/*
void saveNasabahAccount(char inpName[15], char inpTlp, char inpAddr[30], char inpPin){
    char buff[255];
    char text[255];
    FILE *fptr;
    //membuka file untuk ditulis
    fptr=fopen("akunnasabah.txt", "a");
    if(fptr==NULL){
        printf("Error to open! \n");
    }
    struct accountNasabah usr=(%inpName, &inpTlp, &inpAddr, &inpPin);
    fwrite(&usr, sizeof(struct usr), 1, fptr));
    if(fwrite != 0){
        printf("Successfully! \n");
    }else{
        printf("Error reading file! \n");
    }
    fclose(fptr);
    //buka kembali file untuk dibaca
    // baca isi file dengan gets lalu simpan ke buff
    while(fgets(buff, sizeof(buff), fptr)){
        printf("Isi filenya sekarang: %s", buff);
    }
    //tutup file
    fclose(fptr);
    while(fread(&usr, sizeof(struct usr), 1, fptr))){
    }
}
*/

//Push Nasabah
void pushNasabahAccount(char inpName[20], char inpTlp[15], char inpAddr[40], int inpPin){

    current = (struct accountNasabah*) malloc(sizeof(struct accountNasabah));

    strcpy(current->inpName, inpName);
    strcpy(current->inpTlp, inpTlp);
    strcpy(current->inpAddr, inpAddr);
    current->inpPin=inpPin;

    //->next=NULL;

    if(head==NULL){
        head=tail=current;
    }else{
        current->next=head;
        head=current;
    }
}

//Pop Nasabah
void popNasabahAccount(char inpName[20], char inpTlp[15], char inpAddr[40], int inpPin){

}

void setorNasabah(){
    int inpNoCard, inpMoney;
    int inpMenu;

    printf("************************************************************\n");
    printf("*. Silahkan masukan Nomor Kartu Akun yang ingin kamu setorkan uang : ");
    scanf("%d", &inpNoCard);
    printf("*. SIlahkan masukan Jumlah uang yang ingin kamu setorkan uang      : ");
    scanf("%d", &inpMoney);
    printf("\n");
    printf("************************************************************\n");
    printf("\n");
    printf("Setoran uang BERHASIL dilakukan! \n");
    printf("\n");
}

void staffBankMenu(){
    int inpMenu;

    printf("************************************************************\n");
    printf("1. Tambahkan Akun untuk Nasabah yang sudah ada \n");
    printf("2. Hapus Akun untuk Nasabah yang sudah ada \n");
    printf("3. Ubah Informasi Akun \n");
    printf("4. Lakukan Setoran ke Akun Nasabah \n");
    printf("5. Keluar Program \n");
    printf("************************************************************\n");
    printf("\n");
    printf("Masukkan pilihan anda [1-5]: ");
    scanf("%d", &inpMenu);

    switch(inpMenu){
        case 1:
            printf("Satu \n");
            getchar();
            break;
        case 2:
            printf("Dua \n");
            getchar();
            break;
        case 3:
            printf("Tiga \n");
            getchar();
            break;
        case 4:
            system("cls");
            setorNasabah();
            getchar();
            break;
        case 5:
            break;
        default:
            printf("Menu tidak tersedia!");
            getchar();
            break;
    }
}

void aksesInformasiAcc(){
    int inpPin;

    printf("Masukan PIN kamu : \n");
    scanf("%d", &inpPin);
    printf("\n");
    printf("                        ---- Informasi Akun Anda ----                          \n");
    printf("Nomor Kartu         : \n");
    printf("Nama Pemilik Akun   : \n");
    printf("Saldo Akun          : \n");
    printf("Nomor Telepon       : \n");
    printf("Alamat              : \n");
    printf("--------------------------------------------------------------------------------\n");
    printf("\n");
    printf("Tekan tombol apa saja untuk melanjutkan!");
}


//////////nisa////////////////////////////////////////////////////////////////
//Create Akun Manager
void createStaffAccount(){

    char inpNameS[20];
    int inpPinS;


    printf("****************************************************************************************\n");
    printf("                             Menu Staff Bank KEB Hana                                 \n");
    printf("\n");
    printf("Sekarang Kamu sedang membuat Akun Staff, silahkan masukkan data informasi dibawah ini:\n");

    do{
        fflush(stdin);
        printf("*. Masukkan Nama anda (Maksimal 20 digit karakter)          : ");
        scanf("%[^\n]s", inpNameS);
        fflush(stdin);
    }while(strlen(inpNameS) < 1 || strlen(inpNameS) > 20);
    do{
        printf("*. Silahkan buat PIN anda (6 digit angka)                   : ");
        scanf("%d", &inpPinS);
        fflush(stdin);
    }while(strlen(inpPinS) < 1 || strlen(inpPinS) > 6);

    printf("\n");
    printf("                        ---- Akun Berhasil Dibuat ----                          \n");
    printf("Nama                    : %s      \n", inpNameS);
    printf("PIN                     : %d      \n", inpPinS);
    printf("--------------------------------------------------------------------------------\n");
    printf("\n");
    printf("Tekan tombol apa saja untuk melanjutkan!");
    printf("\n");

}
/////////////////////////////////////////////////////////////////////////////
typedef struct{
int TI[nMax+1];
int nEff;
}TabInt;

// Definisi prototype
void getMenu();
void getMenuPilihTransfer();
void getMenuMutasiTrx();
void cetakTabel(TabInt T);
void addElmTab(TabInt *T, int x);
void urutBubbleDesc(TabInt *T);
void urutBubbleAsc(TabInt *T);
void terbilang(long x);

void getMenuPilihTransfer(){ // Menu Pilihan Transfer Dana
    printf("**************** TRANSFER DANA ****************\n");
    printf("* 1. Transfer Dana ke Rekening Bank ABC *\n");
    printf("* 2. Transfer Dana ke Rekening Bank Lain *\n");
    printf("* 3. Kembali ke menu *\n");
    printf("***********************************************\n");
}

void getMenuMutasiTrx(){ // Menu Mutasi Transaksi
    printf("********** MUTASI TRANSAKSI **********\n");
    printf("* 1. Transaksi Terakhir *\n");
    printf("* 2. Urutkan Transaksi Terbesar *\n");
    printf("* 3. Urutkan Transaksi Terkecil *\n");
    printf("* 4. Kembali ke Menu *\n");
    printf("**************************************\n");
}

void addElmTab(TabInt *T, int x){ // Prosedur untuk menambahkan Elemen pada sebuah Tabel yang terdapat pada tabel integer
    /* I.S: T terisi sembarang, mungkin kosong
    F.S: Bertambah 1 elemen bernilai x
    */
    // Algoritma
    if((*T).nEff < nMax){
    (*T).nEff++;
    (*T).TI[(*T).nEff] = x;
    }
}

void cetakTabel(TabInt T){ // Prosedur untuk mencetak isi tabel pada tabel integer
    /* I.S: T Terdefinisi sembarang
    F.S: Semua elemen pada T dicetak
    */
    // Variabel / Kamus
    int i;

    // Algoritma
    i=1;
    while(i<=T.nEff){
    printf("<Rp.%d,->\n", T.TI[i]);
    i++;
    }
}

void urutBubbleDesc(TabInt *T){ // Prosedur Pengurutan bilangan menggunakan metode bubblesort descending (Dari besar ke kecil)
    /* I.S: Mengurutkan bilangan dari yang terbesar hingga terkecil
    F.S: i,j,tmp saling bertukar tempat
    */
    // Variabel / kamus
    int i,j, tmp;
    int N;

    // Algoritma
    N = (*T).nEff;

    for(i=N;i>1;i--){
    for(j=2;j<=i;j++){
    if((*T).TI[j-1] < (*T).TI[j]){
    //pertukaran tempat
    tmp = (*T).TI[j-1];
    (*T).TI[j-1] = (*T).TI[j];
    (*T).TI[j] = tmp;
    }
    }
    }
}

void urutBubbleAsc(TabInt *T){ // Prosedur Pengurutan bilangan menggunakan metode bubblesort ascending (Dari kecil ke besar)
    /* I.S: Mengurutkan bilangan dari yang terkecil hingga terbesar
    F.S: i,j,tmp saling bertukar tempat
    */
    // Variabel / Kamus
    int i,j, tmp;
    int N;

    // Algoritma
    N = (*T).nEff;

    for(i=N;i>1;i--){
    for(j=2;j<=i;j++){
    if((*T).TI[j-1] > (*T).TI[j]){
    //pertukaran tempat
    tmp = (*T).TI[j-1];
    (*T).TI[j-1] = (*T).TI[j];
    (*T).TI[j] = tmp;
    }
    }
    }
}
void terbilang(long x){ // Prosedur mengubah angka menjadi kata
    /* I.S: x mengkonversi nilai x menjadi kata
    F.S: nilai x berubah
    */
    // Algoritma
    if(x==1){
    printf("Satu ");
    }else if(x==2){
    printf("Dua ");
    }else if(x==3){
    printf("Tiga ");
    }else if(x==4){
    printf("Empat ");
    }else if(x==5){
    printf("Lima ");
    }else if(x==6){
    printf("Enam ");
    }else if(x==7){
    printf("Tujuh ");
    }else if(x==8){
    printf("Delapan ");
    }else if(x==9){
    printf("Sembilan ");
    }else if(x==10){
    printf("Sepuluh ");
    }else if(x==11){
    printf("Sebelas ");
    }else if(x>=12&&x<=19){
    terbilang(x%10);
    printf("Belas ");
    }else if(x>=20&&x<=99){
    terbilang(x/10);
    printf("Puluh ");
    terbilang(x%10);
    }else if(x>=100&&x<=199){
    printf("Seratus ");
    terbilang(x-100);
    }else if(x>=200&&x<=999){
    terbilang(x/100);
    printf("Ratus ");
    terbilang(x%100);
    }else if(x>=1000&&x<=1999){
    printf("Seribu ");
    terbilang(x-1000);
    }else if(x>=2000&&x<=999999){
    terbilang(x/1000);
    printf("Ribu ");
    terbilang(x%1000);
    }else if(x>=1000000&&x<=999999999){
    terbilang(x/1000000);
    printf("Juta ");
    terbilang(x%1000000);
    }else if(x>=1000000000&&x<=2147483647){
    terbilang(x/1000000000);
    printf("Miliyar ");
    terbilang(x%1000000000);
    }
}

void nasabahMenu(){
    int inpMenu;
    char y1, y2, y3, y4, y5, y6, y7, y8;
    int i=0, j=0;
    int pin;
    int npin;
    int fpin = 123456;
    int sal = 1000000;
    int N, TR, TF;
    int T=0;
    TabInt Tab1, Tab2;

    login: // Perintah login akan dipanggil oleh syntax "goto"
    system("cls");
    printf("********** BANK ABC **********\n");
    printf("MASUKAN PIN ANDA:\n"); scanf("%d", &pin);
    if (pin==fpin) {
    goto menu;
    }
    else if (i<2) { // diberi kesempatan memasukan PIN sebanyak 3x.
    printf("PIN YANG ANDA MASUKAN SALAH!\n");
    i=i+1;
    getch();
    goto login;
    system("cls");
    }
    else { // apabila 3x salah memasukan PIN.
    printf ("Anda telah 3x salah memasukan PIN.\n");
    printf("Mohon maaf, account anda telah kami blokir.\n");
    printf("Silahkan hubungi Customer Service kami.\n");
    }
    goto exit; // Keluar dari program


    menu:
    printf("************************************************************\n");
    printf("|           Aktivitas yang bisa kamu pilih :               |\n");
    printf("------------------------------------------------------------\n");
    printf("\n");
    printf("1. Penarikan Saldo \n");
    printf("2. Akses ke Informasi Akun \n");
    printf("3. Ubah PIN \n");
    printf("4. Transfer Uang \n");
    printf("5. Akses ke Log Aktivitas \n");
    printf("6. Keluar Program \n");
    printf("************************************************************\n");
    printf("\n");
    printf("Masukkan pilihan anda [1-6]: ");
    scanf("%d", &inpMenu);

    switch(inpMenu){
        case 1:{ // Penarikan Dana
            do {
            jmltarik:
            system("cls");
            printf("*************** TARIK TUNAI ***************\n");
            printf("Masukan Jumlah yang akan ditarik:"); scanf("%d", &TR);
            system("cls");
            printf("*************** TARIK TUNAI ***************\n");
            printf("Jumlah Penarikan:Rp.%d,-\n", TR);
            printf("Terbilang: "); terbilang(TR); printf("Rupiah\n");
            printf("Apakah anda ingin melakukan transaksi ini?(y/t):"); scanf("%s", &y6);
            if (y6=='y' || y6=='Y') {
            goto tarik;
            }
            else {
            goto menu;
            }
            tarik:
            if (TR < 10000) {
            printf("Nominal harus kelipatan Rp.10000,- !");
            getch();
            goto jmltarik;
            } else if (sal < TR) {
            printf("Saldo anda tidak mencukupi. Saldo anda saat ini:Rp.%d,-\n", sal);
            } else if (sal >= TR) {
            sal = sal - TR;
            printf("Transaksi berhasil. Saldo anda sekarang: Rp.%d,-\n", sal);
            addElmTab(&Tab2, TR);
            }
            printf("Ingin melakukan transaksi lagi ?(y/t):"); scanf("%s", &y7);
            }
            while(y7=='y' || y7=='Y');
            goto menu;
            break;
            }
        case 2:
            system("cls");
            aksesInformasiAcc();
            getchar();
            break;
        case 3:
            { // Ganti PIN ATM
            printf("*************** GANTI PIN ***************\n");
            pinlama:
            printf("Masukan PIN lama:"); scanf("%d", &pin);
            if(pin==fpin){
            goto pinbaru;
            }
            else if(j<2){
            printf("PIN LAMA ANDA SALAH!\n");
            j=j+1;
            goto pinlama;
            }
            else{
            printf("AKUN DIBLOKIR! SILAHKAN HUBUNGI CS KAMI.\n");
            goto exit;
            }
            pinbaru:
            printf("Masukan PIN baru:"); scanf("%d", &npin);
            system("cls");
            fpin=npin;
            printf("*************** GANTI PIN ***************\n");
            printf("Ganti PIN berhasil. Silahkan login kembali.\n");
            getch();
            goto login;
            break;
            }
        case 4:{ // Transfer Dana
            do {
            pilihtransfer:
            system("cls"); // Membersihkan layar
            getMenuPilihTransfer(); // Memanggil prosedur Menu Pilih Transfer
            printf("Masukan Pilihan:"); scanf("%s", &y2);
            system("cls");
            switch(y2) {
            case '1': { // Transfer dana ke rekening Bank ABC
            printf("*************** KE REKENING BANK ABC ***************\n");
            printf("Masukan No.Rek Tujuan:"); scanf("%d", &T);
            system("cls");
            printf("*************** KE REKENING BANK ABC ***************\n");
            printf("Masukan Nominal yang akan ditransfer:"); scanf("%d", &N);
            system("cls");
            printf("*************** KE REKENING BANK ABC ***************\n");
            printf("Rekening Tujuan:%d\n", T);
            printf("Jumlah Transfer:%d\n", N);
            printf("Terbilang:"); terbilang(N); printf("Rupiah\n");
            printf("Apakah anda ingin melakukan transaksi ini?(y/t):"); scanf("%s", &y3);
            if (y3=='y' || y3=='Y') {
            goto transferabc;
            }
            else {
            goto menu;
            }
            transferabc:
            if (sal < N) {
            printf("Saldo anda tidak mencukupi. Saldo anda saat ini:Rp.%d,-\n", sal);
            }
            else if (sal>=N) {
            sal = sal - N;
            printf("Transaksi berhasil. Saldo anda sekarang:Rp.%d,-\n", sal);
            addElmTab(&Tab1, N);
            }
            break;
            }
            case '2': { // Transfer dana ke rekening bank lain
            printf("*************** KE REKENING BANK LAIN ***************\n");
            printf("Masukan No.Rek Tujuan:"); scanf("%d", &T);
            system("cls");
            printf("*************** KE REKENING BANK LAIN ***************\n");
            printf("Masukan Nominal yang akan ditransfer:"); scanf("%d", &N);
            system("cls");
            printf("*************** KE REKENING BANK LAIN ***************\n");
            printf("Rekening Tujuan:%d\n", T);
            printf("Jumlah Transfer:Rp.%d,-\n", N);
            printf("Terbilang: "); terbilang(N); printf("Rupiah\n");
            printf("Biaya Transfer ke rekening bank lain: Rp.7500,-\n");
            printf("Apakah anda ingin melakukan transaksi ini?(y/t):"); scanf("%s", &y4);
            if (y4=='y' || y4=='Y'){
            goto transferlain;
            }
            else {
            goto menu;
            }
            transferlain:
            TF=7500; // Biaya transfer ke rekening bank lain
            if (sal < N) {
            printf("Saldo anda tidak mencukupi. Saldo anda saat ini:Rp.%d,-\n", sal);
            }
            else if (sal>=N) {
            sal = sal - N - TF;
            printf("Transaksi berhasil. Saldo anda sekarang:Rp.%d,-\n", sal);
            addElmTab(&Tab1, N);
            addElmTab(&Tab1, TF);
            }
            break;
            }
            case '3': { // Kembali ke menu utama
            goto menu;
            break;
            }
            default : { // Memasukan input selain angka 1-3
            getMenuPilihTransfer();
            printf("Input Salah(Masukan angka 1-3)");
            getch();
            goto pilihtransfer;
            break;
            }
            }
            printf("Ingin transfer lagi? (y/t):"); scanf("%s", &y5);
            system("cls");
            }
            while (y5=='y' || y5=='Y');
            goto menu;
            break;
            }
        case 5:
            { // Histori Transaksi
            mutasitrx:
            system("cls");
            getMenuMutasiTrx();
            printf("Masukan pilihan:"); scanf("%s", &y8);
            system("cls");
            switch (y8) {
            case '1': { // Transaksi Terakhir
            printf ("***************** TRANSAKSI TERAKHIR ***************\n");
            printf("********* TRANSFER DANA *********\n");
            cetakTabel(Tab1); // Mencetak isi Tabel pada TabInt Tab1
            printf("Ke: %d\n", T);
            printf("*********************************\n");
            printf("******** PENARIKAN DANA *********\n");
            cetakTabel(Tab2); // Mencetak isi Tabel pada TabInt Tab2
            printf("*********************************\n");
            printf("<< Kembali ke menu");
            break;
            }
            case '2': { // Mengurutkan transaksi dari yang terbesar hingga terkecil
            printf("********** TRANSAKSI TERBESAR **********\n");
            printf("********** TRANSFER DANA ***************\n");
            urutBubbleDesc(&Tab1);
            cetakTabel(Tab1);
            printf("************ PENARIKAN DANA ************\n");
            urutBubbleDesc(&Tab2);
            cetakTabel(Tab2);
            printf("****************************************\n");
            printf("<< Kembali ke menu");
            break;
            }
            case '3': { // Mengurutkan transaksi dari yang terkecil hingga terbesar
            printf("********** TRANSAKSI TERKECIL **********\n");
            printf("************* TRANSFER DANA ************\n");
            urutBubbleAsc(&Tab1);
            cetakTabel(Tab1);
            printf("****************************************\n");
            printf("************ PENARIKAN DANA ************\n");
            urutBubbleAsc(&Tab2);
            cetakTabel(Tab2);
            printf("****************************************\n");
            printf("<< Kembali ke menu");
            break;
            }
            case '4': { // Kembali ke menu utama
            goto menu;
            break;
            }
            default : { //
            getMenuMutasiTrx();
            printf("Input Salah(Masukan angka 1-4)");
            getch();
            goto mutasitrx;
            break;
            }
            }
            getch();
            goto menu;
            break;
            }
        case 6:
            exit(0);
            break;
            getch();
        default:
            printf("Menu tidak tersedia!");
            getchar();
            break;
    }
        exit: // Keluar program dengan nilai return 0
        return 0;
}

void createNasabahAccount(){
    char inpName[20];
    char inpTlp[15];
    char inpAddr[40];
    int inpPin;
    int rand(void);

    printf("****************************************************************************************\n");
    printf("                             Menu Nasabah Bank KEB Hana                                 \n");
    printf("\n");
    printf("Sekarang Kamu sedang membuat Akun Nasabah, silahkan masukkan data informasi dibawah ini:\n");

    do{
        fflush(stdin);
        printf("*. Masukkan Nama anda (Maksimal 20 digit karakter)          : ");
        scanf("%[^\n]s", inpName);
        fflush(stdin);
    }while(strlen(inpName) < 1 || strlen(inpName) > 20);

    do{
        printf("*. Masukkan Nomor Telepon anda (Maksimal 15 digit angka)    : ");
        fflush(stdin);
        scanf("%d", &inpTlp);
        fflush(stdin);
    }while(strlen(inpTlp) < 1 || strlen(inpTlp) > 15);

    do{
        printf("*. Masukkan Alamat anda (Maksimal 40 digit karakter)        : ");
        scanf("%[^\n]s", inpAddr);
        fflush(stdin);
    }while(strlen(inpAddr) < 1 || strlen(inpAddr) > 40);

    do{
        printf("*. Silahkan buat PIN anda (6 digit angka)                   : ");
        scanf("%d", &inpPin);
        fflush(stdin);
    }while(strlen(inpPin) < 1 || strlen(inpPin) > 6);

    printf("\n");
    printf("Sekarang anda diberikan Nomor kartu yang digunakan untuk login, HARAP DICATAT!! \n");
    printf("\n");
    printf("                        ---- Akun Berhasil Dibuat ----                          \n");
    printf("Nama                    : %s      \n", inpName);
    printf("Nomor Telepon           : %d      \n", inpTlp);
    printf("Alamat                  : %s      \n", inpAddr);
    printf("PIN                     : %d      \n", inpPin);
    printf("Nomor Kartu anda adalah : %d      \n", rand());
    printf("--------------------------------------------------------------------------------\n");
    printf("\n");
    printf("Tekan tombol apa saja untuk melanjutkan!");
    printf("\n");

    //Save or push!
    //saveNasabahAccount(inpName, inpTlp, inpAddr, inpPin);
   // nasabahAccount(inpName, inpAddr, inpTlp, inpPin);
    printf("Data berhasil disimpan di list! \n");
    printf("Isi dari list anda : \n");
    printList();
}

void printList(){
    current = head; //set current sebagai head
	while(current != NULL){ //looping selama current bukan null
		printf("Nama anda    : %s -> %d\n",current->inpName); //tampilkan nilai nama dan nim
		printf("Address anda : %s -> %d\n",current->inpAddr);
		current = current->next; //set current menjadi nilai setelahnya
	}

    /*
	while(ptr != NULL){ //looping selama current bukan null
		printf("Nama anda    -> %d \n",ptr->inpName); //tampilkan nilai nama dan nim
		printf("Address anda -> %d \n",ptr->inpAddr);
		ptr = ptr->next;
	}
    printf(" [null] \n");
    */
}

////////kerjaan nisa//////////////////////////////////////////////////////////////////

//Struct Manager
struct accountManager {
    char inpNameM[20];
    int inpPinM;

    struct accountManager *next;
} *headM, *tailM, *currentM;

//Push Manager
void pushManagerAccount(char inpNameM[20], int inpPinM){

    currentM = (struct accountManager*) malloc(sizeof(struct accountManager));

    strcpy(currentM->inpNameM, inpNameM);
    currentM->inpPinM=inpPinM;

    //->next=NULL;

    if(headM==NULL){
        headM=tailM=currentM;
    }else{
        currentM->next=headM;
        headM=currentM;
    }
}

//Pop Manager
void popManagerAccount(char inpNameM[20], int inpPinM){

}

void loginManager(){
    int inpNameM;
    int inpPinM;
    printf("Masukan Username : ");
    scanf("%s",&inpNameM);
    printf("Masukan Password : ");
    scanf("%s",&inpPinM);
    if(strcmp(inpNameM,"nisamn")==0 && strcmp(inpPinM,"280398")==0){
        printf("Akses diterima. Selamat Datang\n");
        system("pause");
        ManagerMenu();
    }
    else{
         printf("Username dan Password tidak match\n");
         system("pause");
         exit(1);
    }
}


//1. Akses Informasi Akun Nasabah
void aksesInformasi(){
    int inpName, inpTlp, inpAddr, inpPin, rand;
    printf("Masukkan Nama Nasabah: \n");
    scanf("%s",&inpName);
    printf("                      ---- Akun Nasabah yang dicari ----                        \n");
    printf("Nama                    : %s      \n", inpName);
    printf("Nomor Telepon           : %d      \n", inpTlp);
    printf("Alamat                  : %s      \n", inpAddr);
    printf("PIN                     : %d      \n", inpPin);
    //printf("Nomor Kartu anda adalah : %d      \n", rand());
    printf("--------------------------------------------------------------------------------\n");
}
//2. Block or unblock akun
void blockUnblock(){
    int inpName;
    printf("Masukkan Nama Nasabah yang ingin diblock: \n");
    scanf("%s",&inpName);
    printf("Apakah anda ingin mem block akun ini: \n");
}
//3. Menghitung Total jumlah Nasabah
void totalJumlahNasabah(){
    printf("Total jumlah Nasabah adalah: \n");
}
//4. Menghitung Total akun nasabah
void totalAkunNasabah(){
    printf("Total jumlah Akun Nasabah adalah: \n");
}
//5. Rata-rata Akun per Nasabah
void ratarataAkun(){
    printf("Rata-rata jumlah Akun Nasabah adalah: \n");
}
//6. Total Saldo seluruh akun
void totalSaldo(){
    printf("Total Saldo keseluruhan adalah: \n");
}
//7. Rata Rata saldo seluruh akun
void ratarataSaldo(){
    printf("Rata-rata saldo adalah: \n");
}

//Menu pada Manager
void ManagerMenu(){
    int inpMenu;

    printf("************************************************************\n");
    printf("|           Silakan Pilih Menu yang tersedia :               |\n");
    printf("------------------------------------------------------------\n");
    printf("\n");
    printf("1. Akses Informasi Akun Nasabah \n");
    printf("2. Block or unblock Akun \n");
    printf("3. Jumlah Nasabah \n");
    printf("4. Jumlah Akun \n");
    printf("5. Rata-rata Akun per Nasabah \n");
    printf("6. Total Saldo Akun \n");
    printf("7. Rata-rata Saldo Akun \n");
    printf("8. Keluar Program \n");
    printf("************************************************************\n");
    printf("\n");
    printf("Masukkan pilihan anda [1-8]: ");
    scanf("%d", &inpMenu);

    switch(inpMenu){
        case 1:
            system("cls");
            aksesInformasi();
            getchar();
            break;
        case 2:
            system("cls");
            blockUnblock();
            getchar();
            break;
        case 3:
            system("cls");
            totalJumlahNasabah();
            getchar();
            break;
        case 4:
            system("cls");
            totalAkunNasabah();
            getchar();
            break;
        case 5:
            system("cls");
            ratarataAkun();
            getchar();
            break;
        case 6:
            system("cls");
            totalSaldo();
            getchar();
            break;
        case 7:
            system("cls");
            ratarataSaldo();
            getchar();
            break;
        case 8:
            exit(0);
            break;
            getch();
        default:
            printf("Menu tidak tersedia!");
            getchar();
            break;
    }
}

//Create Akun Manager
void createManagerAccount(){
    char inpNameM[20];
    int inpPinM;
    int rand(void);

    printf("****************************************************************************************\n");
    printf("                             Menu Manager Bank KEB Hana                                 \n");
    printf("\n");
    printf("Sekarang Kamu sedang membuat Akun Manager, silahkan masukkan data informasi dibawah ini:\n");

    do{
        fflush(stdin);
        printf("*. Masukkan Nama anda (Maksimal 20 digit karakter)          : ");
        scanf("%[^\n]s", inpNameM);
        fflush(stdin);
    }while(strlen(inpNameM) < 1 || strlen(inpNameM) > 20);
    do{
        printf("*. Silahkan buat PIN anda (6 digit angka)                   : ");
        scanf("%d", &inpPinM);
        fflush(stdin);
    }while(strlen(inpPinM) < 1 || strlen(inpPinM) > 6);

    printf("\n");
    printf("                        ---- Akun Berhasil Dibuat ----                          \n");
    printf("Nama                    : %s      \n", inpNameM);
    printf("PIN                     : %d      \n", inpPinM);
    printf("--------------------------------------------------------------------------------\n");
    printf("\n");
    printf("Tekan tombol apa saja untuk melanjutkan!");
    printf("\n");

    //Save or push!
    //saveNasabahAccount(inpName, inpTlp, inpAddr, inpPin);
   // nasabahAccount(inpName, inpAddr, inpTlp, inpPin);
    printf("Data berhasil disimpan di list! \n");
    printf("Isi dari list anda : \n");
    printList();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//display menu Awal
void displayMenu(){
    time_t now = time(NULL);
    char *string_now = ctime(&now);
    int inpMenu;

    back:
    system("cls");
    printf("**************************************************************\n");
    printf("|               Selamat Datang di Bank KEB Hana              |\n");
    printf("|                                                            |\n");
    printf("|------------------------------------------------------------|\n");
    printf("| Tanggal dan Waktu (UTC+7): %s                               \n", string_now);
    printf("|------------------------------------------------------------|\n");
    printf("\n");
    printf("Silakan Pilih Menu : \n");
    printf("**************************************************************\n");
    printf("1. Membuat Akun Nasabah                                      |\n");
    printf("2. Masuk sebagai Nasabah                                     |\n");
    printf("3. Membuat Akun Staf Bank                                    |\n");
    printf("4. Masuk Sebagai Staf Bank                                   |\n");
    printf("5. Membuat Akun Manager Bank                                 |\n");
    printf("6. Masuk sebagai Manager Bank                                |\n");
    printf("7. Keluar Program                                            |\n");
    printf("**************************************************************\n");
    printf("\n");
    printf("Masukkan pilihan anda [1-6]: ");
    scanf("%d", &inpMenu);
    fflush(stdin);

    switch(inpMenu){
        case 1:
            system("cls");
            createNasabahAccount();
            getchar();
            goto back;
            break;
        case 2:
            system("cls");
            nasabahMenu();
            getchar();
            goto back;
            break;
        case 3:
            system("cls");
            createStaffAccount();
            getchar();
            goto back;
            break;
        case 4:
            system("cls");
            staffBankMenu();
            getchar();
            goto back;
            break;
        case 5:
            system("cls");
            createManagerAccount();
            getchar();
            goto back;
            break;
        case 6:
            system("cls");
            ManagerMenu();
            getchar();
            goto back;
            break;
        case 7:
            exit(0);
            break;
            getch();
        default:
            printf("Menu tidak tersedia!");
            break;
    }
}

int main()
{
    displayMenu();
    return 0;
}

